package com.autotest.seleniumjava;

import com.autotest.seleniumjava.Page.BasePage;
import org.junit.After;
import org.junit.Before;

/**
 * Created by LENOVO on 09/07/2017.
 */
public class BaseTest extends BasePage {

    @Before
    public void beforeTest(){
        DriverHelper.Default.openBrowser(TestData.BROWSER_EDGE);
        DriverHelper.Default.goToSite();
    }

    @After
    public void afterTest(){
//        DriverHelper.Default.quit();
        assertFail();
    }
}
