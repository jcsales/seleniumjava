package com.autotest.seleniumjava;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.io.File;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by LENOVO on 08/07/2017.
 */
public class DriverHelper {
    public static final DriverHelper Default = new DriverHelper();
    private static WebDriver driver;
    private static final String FF_PATH = "src\\geckodriver.exe";
    private static final String CHROME_PATH = "src\\chromedriver.exe";
    private static final String IE_PATH = "src\\IEDriverServer.exe";
    private static final String EDGE_PATH = "src\\MicrosoftWebDriver.exe";
    private static final String BINARY_CHROME = "webdriver.chrome.driver";
    private static final String BINARY_FF = "webdriver.gecko.driver";
    private static final String BINARY_IE = "webdriver.ie.driver";
    private static final String BINARY_EDGE = "webdriver.edge.driver";



    public void openBrowser(String browser) {
        if(browser.equals(TestData.BROWSER_CHROME)){
            File file = new File(CHROME_PATH);
            System.setProperty(BINARY_CHROME, file.getAbsolutePath());
            driver = new ChromeDriver();
        } else if(browser.equals(TestData.BROWSER_FF)){
            File file = new File(FF_PATH);
            System.setProperty(BINARY_FF, file.getAbsolutePath());
            driver = new FirefoxDriver();
        }else if(browser.equals(TestData.BROWSER_IE)){
            File file = new File(IE_PATH);
            System.setProperty(BINARY_IE, file.getAbsolutePath());
            driver = new InternetExplorerDriver();
        }else if(browser.equals(TestData.BROWSER_EDGE)){
            File file = new File(EDGE_PATH);
            System.setProperty(BINARY_EDGE, file.getAbsolutePath());
            driver = new EdgeDriver();
        }
        driver.manage().window().maximize();
    }

    public void quit(){
        driver.close();
        driver.quit();
    }

    public WebDriver getWebDriver() {
        return this.driver;
    }

    public static WebElement findElement(By by){
        try{
            WebElement element = WaitUtil.fluentWait().until(ExpectedConditions.presenceOfElementLocated(by));
            return element;
        } catch (Exception e){
            System.out.println("No element found");
            return null;
        }
    }

    public static List<WebElement> findElements (By by){
        try {
            return WaitUtil.fluentWait().until(
                    e -> DriverHelper.Default.getWebDriver().findElements(by));
        } catch (NoSuchElementException | NullPointerException e) {
            System.out.println("No element found");
            return null;
        }
    }

    public static void click(WebElement e){
        WebElement element = WaitUtil.fluentWait().until(ExpectedConditions.elementToBeClickable(e));
        element.click();
    }

    public static void sendKeys(WebElement element, String text){
        WebElement e = WaitUtil.fluentWait().until(ExpectedConditions.elementToBeClickable(element));
        if (e!=null && e.isDisplayed()){
            e.clear();
            e.sendKeys(text);
        }else
            System.out.println(e + " Element is null or not displayed");
    }

    public static void select(WebElement element, String value){
        try {
            Select selector = new Select(element);
            selector.selectByVisibleText(value);
        } catch (Exception e){
            System.out.println("No matching option element was found");
        }
    }

    public static WebElement findElement(WebElement parentElement, By by) {
        try {
            WebElement element = parentElement.findElement(by);
            //focusElement(element);
            return element;
        } catch (NoSuchElementException | NullPointerException e) {
            return null;
        }
    }

    public static WebElement getParent(WebElement e) {
        return findElement(e, new By.ByXPath(".."));
    }

    public void goToSite(){
//        Default.driver.get("https://www.ss.lv");
        Default.driver.get("https://www.tst1.bookwana.com/");
    }

}
