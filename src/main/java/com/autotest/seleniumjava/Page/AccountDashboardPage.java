package com.autotest.seleniumjava.Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by jsales on 9/12/2017.
 */
public class AccountDashboardPage extends BasePage {
    private static String ID_CREATE_EVENT_LINK = "ctl00_ContentPlaceHolder1_hlEventCreate";

    private WebElement findCreateEventLink(){
        return this.findElement(By.id(ID_CREATE_EVENT_LINK));
    }

    public void clickCreateEventLink(){
        this.click(this.findCreateEventLink());
    }

}
