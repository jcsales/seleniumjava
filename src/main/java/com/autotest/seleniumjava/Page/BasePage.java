package com.autotest.seleniumjava.Page;

import com.autotest.seleniumjava.DriverHelper;
import com.autotest.seleniumjava.TestData;
import com.autotest.seleniumjava.WaitUtil;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by LENOVO on 08/07/2017.
 */
public class BasePage extends DriverHelper {

    public final StringBuilder errorMessage = new StringBuilder();

    //region CONSTANTS
    private static final String LINK_TEXT_RU_LANG = "RU";
    private static final String LINK_TEXT_ELEC_ENGR = "Электротехника";
    private static final String LINK_TEXT_SEARCH = "Поиск";
    private static final String LINK_TEXT_SORT_BY_PRICE = "Цена";
    private static final String LINK_TEXT_ADVANCED_SEARCH = "Расширенный поиск";
    private static final String LINK_TEXT_ADD_TO_MEMO = "Добавить выбранные в закладки";
    private static final String LINK_TEXT_BOOKMARKS = "Закладки";
    private static final String ID_SEARCH_FIELD = "ptxt";
    private static final String ID_ALERT_OK_BUTTON = "alert_ok";
    private static final String NAME_PRICE_MIN_FIELD = "topt[8][min]";
    private static final String NAME_PRICE_MAX_FIELD = "topt[8][max]";
    private static final String NAME_SUBHEADING_DROPDOWN = "sid";
    private static final String NAME_SEARCH_PERIOD_DROPDOWN = "pr";
    private static final String NAME_SORT_BY_DROPDOWN = "sort";
    private static final String ID_TOWN_REGION_DROPDOWN = "s_region_select";
    private static final String ID_SEARCH_BUTTON = "sbtn";
    private static final String ID_BOOKMARK_FORM = "filter_frm";
    private static final String XPATH_TRANSACTION_TYPE_DROPDOWN = "//*[@id=\"page_main\"]/tbody/tr/td/div[2]/span[3]/select";
    private List<String> adsDescription = new ArrayList<>();
    private List<String> adsPrice = new ArrayList<>();
    private List<String> bookmarkAdDescription = new ArrayList<>();
    private List<String> bookmarkAdPrice = new ArrayList<>();
    //endregion

    //region FIND ELEMENTS
    private WebElement findRussianLang() {
        return findElement(By.linkText(LINK_TEXT_RU_LANG));
    }

    private WebElement findElectricalEngrLink() {
        return findElement(By.linkText(LINK_TEXT_ELEC_ENGR));
    }

    private WebElement findSearchLink() {
        return findElement(By.linkText(LINK_TEXT_SEARCH));
    }

    private WebElement findSearchField() {
        return findElement(By.id(ID_SEARCH_FIELD));
    }

    private WebElement findPriceMinField() {
        return findElement(By.name(NAME_PRICE_MIN_FIELD));
    }

    private WebElement findPriceMaxField() {
        return findElement(By.name(NAME_PRICE_MAX_FIELD));
    }

    private WebElement findSubHeadingDropdown() {
        return findElement(By.name(NAME_SUBHEADING_DROPDOWN));
    }

    private WebElement findTownRegionDropdown() {
        return findElement(By.id(ID_TOWN_REGION_DROPDOWN));
    }

    private WebElement findSearchForPeriodDropdown() {
        return findElement(By.name(NAME_SEARCH_PERIOD_DROPDOWN));
    }

    private WebElement findSortByDropdown() {
        return findElement(By.name(NAME_SORT_BY_DROPDOWN));
    }

    private WebElement findSearchButton() {
        return findElement(By.id(ID_SEARCH_BUTTON));
    }

    private WebElement findSortByPriceLink() {
        return findElement(By.linkText(LINK_TEXT_SORT_BY_PRICE));
    }

    private WebElement findTransactionDropdown() {
        return findElement(By.xpath(XPATH_TRANSACTION_TYPE_DROPDOWN));
    }

    private WebElement findAdvancedSearchLink() {
        return findElement(By.linkText(LINK_TEXT_ADVANCED_SEARCH));
    }

    private WebElement findSearchResultsTable() {
        return findElement(By.cssSelector("#page_main > tbody > tr > td > table:nth-child(4)"));
    }

    private WebElement findBookmarksForm() {
        return findElement(By.id(ID_BOOKMARK_FORM));
    }

    private List<WebElement> findBookmarkTables() {
        return findBookmarksForm().findElements(By.tagName("table"));
    }

    private WebElement findBookmarkAdDescription(int table_index, int row_index) {
        return findBookmarkTables().get(table_index).findElements(By.tagName("tr")).get(row_index).findElements(By.tagName("td")).get(2);
    }

    private WebElement findBookmarkAdPrice(int table_index, int row_index) {
        List<WebElement> findPrice = findBookmarkTables().get(table_index).findElements(By.tagName("tr")).get(row_index).findElements(By.tagName("td"));
        int price_index = findPrice.size() - 1;
        return findPrice.get(price_index);
    }

    private List<WebElement> findTableRows() {
        return findSearchResultsTable().findElements(By.tagName("tr"));
    }

    private WebElement findTableCheckbox(int index) {
        return findTableRows().get(index).findElement(By.tagName("input"));
    }

    private WebElement findAdDescription(int index) {
        return findTableRows().get(index).findElements(By.tagName("td")).get(2);
    }

    private WebElement findAdPrice(int index) {
        return findTableRows().get(index).findElements(By.tagName("td")).get(3);
    }

    private WebElement findAddToMemoLink() {
        return findElement(By.linkText(LINK_TEXT_ADD_TO_MEMO));
    }

    private WebElement findAlertOkButton() {
        return findElement(By.id(ID_ALERT_OK_BUTTON));
    }

    private WebElement findBookmarksLink() {
        return findElement(By.linkText(LINK_TEXT_BOOKMARKS));
    }


    //endregion



    //region NAVIGATION
    public void goToUrl(String url) {
        getWebDriver().get(url);
    }

    public void goToEventDashboard(String eid) {
        WaitUtil.sleep(3000);
        this.goToUrl(TestData.URL_EVENT_DASHBOARD + eid);
    }

    public void goToEventLandingPageAsTicketBuyer(String eid){
        this.goToUrl(TestData.URL_EVENT_LANDING_PAGE + eid);
    }

    public void deleteDateTime(WebElement element) {
        WaitUtil.sleep(1000);
        try {
            int length = element.getAttribute("value").length();
            click(element);
            Robot robot = new Robot();
        for(int i=0; i<=length; i++) {
            robot.keyPress(KeyEvent.VK_SHIFT);
            robot.keyRelease(KeyEvent.VK_BACK_SPACE);
            robot.keyPress(KeyEvent.VK_BACK_SPACE);
            robot.keyRelease(KeyEvent.VK_SHIFT);
        }
        } catch (AWTException e) {

            e.printStackTrace();
        }
    }

    public void clickOkOnConfirmation(){
        try{
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
        }catch (AWTException e){
            e.printStackTrace();
        }
    }

    public void inputTime(String time){
        this.inputTimeDetail(time,0,1);
        this.inputTimeDetail(time,1, 2);
        this.inputTimeDetail(time,2,3);
        this.inputTimeDetail(time,3,4);
    }

    public void inputTimeDetail(String time, int begin, int end) {
        String t = time.substring(begin, end);
        try {
            Robot robot = new Robot();
        switch (t){
            case "0":
                robot.keyRelease(KeyEvent.VK_0);
                robot.keyPress(KeyEvent.VK_0);
                break;
            case "1":
                robot.keyRelease(KeyEvent.VK_1);
                robot.keyPress(KeyEvent.VK_1);
                break;
            case "2":
                robot.keyRelease(KeyEvent.VK_2);
                robot.keyPress(KeyEvent.VK_2);
                break;
            case "3":
                robot.keyRelease(KeyEvent.VK_3);
                robot.keyPress(KeyEvent.VK_3);
                break;
            case "4":
                robot.keyRelease(KeyEvent.VK_4);
                robot.keyPress(KeyEvent.VK_4);
                break;
            case "5":
                robot.keyRelease(KeyEvent.VK_5);
                robot.keyPress(KeyEvent.VK_5);
                break;
            case "6":
                robot.keyRelease(KeyEvent.VK_6);
                robot.keyPress(KeyEvent.VK_6);
                break;
            case "7":
                robot.keyRelease(KeyEvent.VK_7);
                robot.keyPress(KeyEvent.VK_7);
                break;
            case "8":
                robot.keyRelease(KeyEvent.VK_8);
                robot.keyPress(KeyEvent.VK_8);
                break;
            case "9":
                robot.keyRelease(KeyEvent.VK_9);
                robot.keyPress(KeyEvent.VK_9);
                break;
        }
        } catch (AWTException e) {

            e.printStackTrace();
        }

    }

    public String formatDate(int daysToAdd, String format){
        DateFormat dateFormat = new SimpleDateFormat(format);
        Calendar cal = Calendar.getInstance();
        if(daysToAdd != 0) {
            cal.add(Calendar.DATE, daysToAdd);
        }
        return dateFormat.format(cal.getTime());
    }

    public String formatDate(int daysToAdd){
        DateFormat dateFormat = new SimpleDateFormat(TestData.DATE_FORMAT_ddMMyyyy);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, daysToAdd);
        return dateFormat.format(cal.getTime());
    }


    //endregion

    //region OTHER METHODS

    private String getAdDescription(int index){
        return findAdDescription(index).findElements(By.tagName("div")).get(1).getText();
    }

    private String getAdPrice(int index){
        return findAdPrice(index).getText();
    }


    private List<String> getBookmarkAdDescription(){
        for(int i = 0; i <= findBookmarkTables().size()-1; i++){
            for(int x = 1; x <= findBookmarkTables().get(i).findElements(By.tagName("tr")).size() - 1; x++ )
                bookmarkAdDescription.add(findBookmarkAdDescription(i, x).getText());
        }
        return bookmarkAdDescription;
    }

    private List<String> getBookmarkAdPrice(){
        for(int i = 0; i <= findBookmarkTables().size()-1; i++){
            for(int x = 1; x <= findBookmarkTables().get(i).findElements(By.tagName("tr")).size() - 1; x++ )
                bookmarkAdPrice.add(findBookmarkAdPrice(i, x).getText());
        }
        return bookmarkAdPrice;
    }

    public BasePage switchToRussianLang(){
        click(findRussianLang());
        return new BasePage();
    }

    public BasePage goToElecEngrSection(){
        click(findElectricalEngrLink());
        return new BasePage();
    }

    public BasePage clickSearchLink(){
        click(findSearchLink());
        return new BasePage();
    }

    public BasePage clickSearchButton(){
        WaitUtil.sleep(2000);
        click(findSearchButton());
        return new BasePage();
    }

    public BasePage clickAdvancedSearchLink(){
        click(findAdvancedSearchLink());
        return new BasePage();
    }

    public BasePage sortByPrice(){
        click(findSortByPriceLink());
        return new BasePage();
    }
    public BasePage inputTextToSearch(String text){
        sendKeys(findSearchField(), text);
        return new BasePage();
    }

    public BasePage inputMinPrice(String value){
        sendKeys(findPriceMinField(), value);
        return new BasePage();
    }

    public BasePage inputMaxPrice(String value){
        sendKeys(findPriceMaxField(), value);
        return new BasePage();
    }

    public BasePage selectSubheading(String value){
        select(findSubHeadingDropdown(), value);
        return new BasePage();
    }

    public BasePage selectTownRegion(String value){
        select(findTownRegionDropdown(), value);
        return new BasePage();
    }

    public BasePage selectSearchForPeriod(String value){
        select(findSearchForPeriodDropdown(), value);
        return new BasePage();
    }

    public BasePage selectSortByOption(String value){
        select(findSortByDropdown(), value);
        return new BasePage();
    }

    public BasePage selectTransactionType(String value){
        select(findTransactionDropdown(), value);
        return new BasePage();
    }

    public List<String> getAdDescriptionList(int index){
        adsDescription.add(getAdDescription(index));
        return adsDescription;
    }

    public List<String> getAdPriceList(int index){
        adsPrice.add(getAdPrice(index));
        return adsPrice;
    }

    public void selectRandomAdsByIndex(int index){
        getAdDescriptionList(index);
        getAdPriceList(index);
        click(findTableCheckbox(index));
    }



    public void addToMemo(){
        click(findAddToMemoLink());
        clickAlertOkButton();
    }

    public void openBookmarks(){
        click(findBookmarksLink());
    }

    public BasePage clickAlertOkButton(){
        WaitUtil.sleep(2000);
        click(findAlertOkButton());
        return new BasePage();
    }

    public boolean doesAdDetailMatch(int index){
           for(int x = 0; x <= adsDescription.size(); x++){
               if(adsDescription.get(index-1).contains(bookmarkAdDescription.get(x))){
                   if(adsPrice.get(index-1).equals(bookmarkAdPrice.get(x))){
                        return true;
                   }
               }
           }
       return false;
    }
    //endregion



    //region ASSERTION

    public void assertFail(){
        if(errorMessage.length() > 0){
            Assert.fail(errorMessage.toString());
        }
    }

    public void assertTrue(boolean value, String message){
        if (value != true)
            errorMessage.append("\n" + message);
    }

    public void validateIfAdsMatch(){
        getBookmarkAdDescription();
        getBookmarkAdPrice();
        //index starts with 1
        this.assertTrue(this.doesAdDetailMatch(1), "Ad 1 details don't match");
        this.assertTrue(this.doesAdDetailMatch(2), "Ad 2 details don't match");
        this.assertTrue(this.doesAdDetailMatch(3), "Ad 3 details don't match");
    }

    //endregion
}
