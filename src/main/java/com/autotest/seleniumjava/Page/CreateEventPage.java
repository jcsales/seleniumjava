package com.autotest.seleniumjava.Page;

import com.autotest.seleniumjava.TestData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by jsales on 9/12/2017.
 */
public class CreateEventPage extends BasePage{
    private static final String ID_EVENT_NAME_FIELD = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpBasicInfo_txtEventName";
    private static final String ID_EVENT_DESCRIPTION_FIELD = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpBasicInfo_memDescription_I";
    private static final String ID_EVENT_CONTACT_NAME = "txtContactPerson_I";
    private static final String ID_EVENT_CONTACT_PHONE = "txtContactPhone_I";
    private static final String ID_EVENT_CONTACT_EMAIL = "txtContactEmailaddress_I";
    private static final String ID_CREATE_EVENT_NEXT_BUTTON = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpBasicInfo_btnNextBasicInformation";
    private static final String ID_VENUE_TEMPLATE_DROPDOWN = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpEventVenue_ddlVenueList_B-1";
    private static final String ID_VENUE_OPTIONS_TABLE = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpEventVenue_ddlVenueList_DDD_L_LBT";
    private static final String ID_VENUE_NAME_FIELD = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpEventVenue_txtVenueName";
    private static final String ID_VENUE_ADDRESS1_FIELD  = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpEventVenue_txtVenueAddressLine1";
    private static final String ID_VENUE_ADDRESS2_FIELD  = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpEventVenue_txtVenueAddressLine2";
    private static final String ID_VENUE_CITY_FIELD  = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpEventVenue_txtVenueCity";
    private static final String ID_VENUE_STATE_FIELD  = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpEventVenue_txtVenueState";
    private static final String ID_VENUE_POSTCODE_FIELD  = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpEventVenue_txtVenuePostCode";
    private static final String ID_CREATE_VENUE_NEXT_BUTTON = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpEventVenue_btnNextEventVenue";
    private static final String ID_SESSION_START_DATE_FIELD = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpEventInfo_deEventStartDate_I";
    private static final String ID_SESSION_START_TIME_FIELD = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpEventInfo_teEventStartTime_I";
    private static final String ID_SESSION_START_TIME_AM_PM_FIELD = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpEventInfo_ddlEventStartTimeAMPM_B-1";
    private static final String ID_SESSION_TIME_AM = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpEventInfo_ddlEventStartTimeAMPM_DDD_L_LBI0T0";
    private static final String ID_SESSION_TIME_PM = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpEventInfo_ddlEventStartTimeAMPM_DDD_L_LBI1T0";
    private static final String ID_SESSION_TIME_NEXT_BUTTON = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpEventInfo_btnNextEventDates";
    private static final String ID_BOOKING_START_DATE_FIELD = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpBookingDates_deBookingStartDate_I";
    private static final String ID_BOOKING_START_TIME_FIELD = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpBookingDates_teBookingStartTime_I";
    private static final String ID_BOOKING_START_AMPM_DROPDOWN = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpBookingDates_ddlBookingStartTimeAMPM_B-1";
    private static final String ID_BOOKING_START_TIME_AM = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpBookingDates_ddlBookingStartTimeAMPM_DDD_L_LBI0T0";
    private static final String ID_BOOKING_START_TIME_PM = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpBookingDates_ddlBookingStartTimeAMPM_DDD_L_LBI1T0";
    private static final String ID_BOOKING_END_DATE_FIELD = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpBookingDates_deBookingEndDate_I";
    private static final String ID_BOOKING_END_TIME_FIELD = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpBookingDates_teBookingEndTime_I";
    private static final String ID_BOOKING_END_AMPM_DROPDOWN= "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpBookingDates_ddlBookingEndTimeAMPM_B-1";
    private static final String ID_BOOKING_END_TIME_AM = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpBookingDates_ddlBookingEndTimeAMPM_DDD_L_LBI0T0";
    private static final String ID_BOOKING_END_TIME_PM = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpBookingDates_ddlBookingEndTimeAMPM_DDD_L_LBI1T0";
    private static final String ID_BOOKING_WINDOW_NEXT_BUTTON = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpBookingDates_btnNextBookingDates";
    private static final String ID_SEATING_ALLOCATION_TYPE_DROPDOWN = "ctl00_ContentPlaceHolder1_wzdCreateEvent_pnlSeating_ddlEventType_B-1";
    private static final String ID_SEATING_ALLOCATION_SPACE = "ctl00_ContentPlaceHolder1_wzdCreateEvent_pnlSeating_ddlEventType_DDD_L_LBI0T0";
    private static final String ID_SEATING_ALLOCATION_SEAT = "ctl00_ContentPlaceHolder1_wzdCreateEvent_pnlSeating_ddlEventType_DDD_L_LBI1T0";
    private static final String ID_SEATING_MAX_NUMBER_OF_SEAT_FIELD = "ctl00_ContentPlaceHolder1_wzdCreateEvent_pnlSeating_txtAllocatedSpaces";
    private static final String ID_SEATING_DETAILS_NEXT_BUTTON = "ctl00_ContentPlaceHolder1_wzdCreateEvent_pnlSeating_btnNextSeating";
    private static final String ID_SEATING_MAX_ROW_NUMBER_FIELD = "ctl00_ContentPlaceHolder1_wzdCreateEvent_pnlSeating_txtNoOfRows";
    private static final String ID_SEATING_MAX_COLUMN_NUMBER_FIELD = "ctl00_ContentPlaceHolder1_wzdCreateEvent_pnlSeating_txtNoOfSeats";
    private static final String ID_SETUP_OPTIONS_DROPDOWN = "ctl00_ContentPlaceHolder1_wzdCreateEvent_pnlPayment_ddlSetupOptions_B-1";
    private static final String ID_FREE_EVENT_OPTION = "ctl00_ContentPlaceHolder1_wzdCreateEvent_pnlPayment_ddlSetupOptions_DDD_L_LBI0T0";
    private static final String ID_SETUP_TICKET_PRICE_OPTION = "ctl00_ContentPlaceHolder1_wzdCreateEvent_pnlPayment_ddlSetupOptions_DDD_L_LBI1T0";
    private static final String ID_ALLOW_TICKET_BUYERS_CHECKBOX = "ctl00_ContentPlaceHolder1_wzdCreateEvent_pnlPayment_cbAllowTicketReturns_S_D";
    private static final String ID_ADD_TICKET_TYPE_BUTTON = "ctl00_ContentPlaceHolder1_wzdCreateEvent_pnlPayment_btnAddTicketPrice";
    private static final String ID_TICKET_PRICE_NEXT_BUTTON = "ctl00_ContentPlaceHolder1_wzdCreateEvent_pnlPayment_btnNextPaymentDetails";
    private static final String ID_TICKET_TYPE_FIELD = "ctl00_ContentPlaceHolder1_popTicketType_txtEditName2";
    private static final String ID_TICKET_TYPE_DESC_FIELD = "ctl00_ContentPlaceHolder1_popTicketType_txtEditDescription2";
    private static final String ID_TICKET_TYPE_QTY_FIELD = "ctl00_ContentPlaceHolder1_popTicketType_txtEditQuantity2";
    private static final String ID_TICKET_TYPE_AMT_FIELD = "ctl00_ContentPlaceHolder1_popTicketType_txtEditValue2";
    private static final String ID_TICKET_TYPE_STATUS_DROPDOWN = "ctl00_ContentPlaceHolder1_popTicketType_cmbTicketTypeStatus_B-1";
    private static final String ID_TICKET_TYPE_ACTIVE = "ctl00_ContentPlaceHolder1_popTicketType_cmbTicketTypeStatus_DDD_L_LBI0T0";
    private static final String ID_TICKET_TYPE_HIDDEN = "ctl00_ContentPlaceHolder1_popTicketType_cmbTicketTypeStatus_DDD_L_LBI1T0";
    private static final String ID_TICKET_TYPE_SAVE_BUTTON = "ctl00_ContentPlaceHolder1_popTicketType_btnEditUpdate2";
    private static final String ID_CREATE_EVENT_BUTTON = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpOptions2_btnNextOptions2";
    private static final String ID_FINISH_BUTTON = "ctl00_ContentPlaceHolder1_wzdCreateEvent_rpEventCreated_btnFinish";


    private WebElement findEventNameField(){
        return this.findElement(By.id(ID_EVENT_NAME_FIELD));
    }

    private WebElement findEventDescriptionField(){
        return this.findElement(By.id(ID_EVENT_DESCRIPTION_FIELD));
    }

    private WebElement findEventContactNameField(){
        return this.findElement(By.id(ID_EVENT_CONTACT_NAME));
    }

    private WebElement findEventContactPhoneField(){
        return this.findElement(By.id(ID_EVENT_CONTACT_PHONE));
    }

    private WebElement findEventContactEmailField(){
        return this.findElement(By.id(ID_EVENT_CONTACT_EMAIL));
    }

    private WebElement findCreateEventNextButton(){
        return this.findElement(By.id(ID_CREATE_EVENT_NEXT_BUTTON));
    }

    private WebElement findVenueTemplateDropdown(){
        return this.findElement(By.id(ID_VENUE_TEMPLATE_DROPDOWN));
    }

    private WebElement findVenueTemplateOptionsTable(){
        return this.findElement(By.id(ID_VENUE_OPTIONS_TABLE));
    }

    private WebElement findVenueNameField(){
        return this.findElement(By.id(ID_VENUE_NAME_FIELD));
    }

    private WebElement findVenueAddress1Field(){
        return this.findElement(By.id(ID_VENUE_ADDRESS1_FIELD));
    }

    private WebElement findVenueAddress2Field(){
        return this.findElement(By.id(ID_VENUE_ADDRESS2_FIELD));
    }

    private WebElement findVenueCityField(){
        return this.findElement(By.id(ID_VENUE_CITY_FIELD));
    }

    private WebElement findVenueStateField(){
        return this.findElement(By.id(ID_VENUE_STATE_FIELD));
    }

    private WebElement findVenuePostCodeField(){
        return this.findElement(By.id(ID_VENUE_POSTCODE_FIELD));
    }

    private WebElement findCreateVenueNextButton(){
        return this.findElement(By.id(ID_CREATE_VENUE_NEXT_BUTTON));
    }

    private WebElement findSessionStartDateField(){
        return this.findElement(By.id(ID_SESSION_START_DATE_FIELD));
    }

    private WebElement findSessionStartTimeField(){
        return this.findElement(By.id(ID_SESSION_START_TIME_FIELD));
    }

    private WebElement findSessionStartTimeAmPmDropdown(){
        return this.findElement(By.id(ID_SESSION_START_TIME_AM_PM_FIELD));
    }

    private WebElement findSessionTimeNextButton(){
        return this.findElement(By.id(ID_SESSION_TIME_NEXT_BUTTON));
    }

    private WebElement findSessionTimeAm(){
        return this.findElement(By.id(ID_SESSION_TIME_AM));
    }

    private WebElement findSessionTimePm(){
        return this.findElement(By.id(ID_SESSION_TIME_PM));
    }

    private WebElement findBookingStartDateField(){
        return this.findElement(By.id(ID_BOOKING_START_DATE_FIELD));
    }

    private WebElement findBookingStartTimeField(){
        return this.findElement(By.id(ID_BOOKING_START_TIME_FIELD));
    }

    private WebElement findBookingStartTimeDropdown(){
        return this.findElement(By.id(ID_BOOKING_START_AMPM_DROPDOWN));
    }

    private WebElement findBookingStartTimeAm(){
        return this.findElement(By.id(ID_BOOKING_START_TIME_AM));
    }

    private WebElement findBookingStartTimePm(){
        return this.findElement(By.id(ID_BOOKING_START_TIME_PM));
    }

    private WebElement findBookingEndDateField(){
        return this.findElement(By.id(ID_BOOKING_END_DATE_FIELD));
    }

    private WebElement findBookingEndTimeField(){
        return this.findElement(By.id(ID_BOOKING_END_TIME_FIELD));
    }

    private WebElement findBookingEndTimeDropdown(){
        return this.findElement(By.id(ID_BOOKING_END_AMPM_DROPDOWN));
    }

    private WebElement findBookingEndTimeAm(){
        return this.findElement(By.id(ID_BOOKING_END_TIME_AM));
    }

    private WebElement findBookingEndTimePm(){
        return this.findElement(By.id(ID_BOOKING_END_TIME_PM));
    }

    private WebElement findBookingWindowNextButton(){
        return this.findElement(By.id(ID_BOOKING_WINDOW_NEXT_BUTTON));
    }

    private WebElement findSeatingAllocationTypeDropdown(){
        return this.findElement(By.id(ID_SEATING_ALLOCATION_TYPE_DROPDOWN));
    }

    private WebElement findSeatingAllocationSeat(){
        return this.findElement(By.id(ID_SEATING_ALLOCATION_SEAT));
    }

    private WebElement findSeatingAllocationSpace(){
        return this.findElement(By.id(ID_SEATING_ALLOCATION_SPACE));
    }

    private WebElement findSeatingMaxNumberOfAttendees(){
        return this.findElement(By.id(ID_SEATING_MAX_NUMBER_OF_SEAT_FIELD));
    }

    private WebElement findSeatingRowNumber(){
        return this.findElement(By.id(ID_SEATING_MAX_ROW_NUMBER_FIELD));
    }

    private WebElement findSeatingColumnNumber(){
        return this.findElement(By.id(ID_SEATING_MAX_COLUMN_NUMBER_FIELD));
    }

    private WebElement findSeatingDetailsNextButton(){
        return this.findElement(By.id(ID_SEATING_DETAILS_NEXT_BUTTON));
    }

    private WebElement findSetupOptionsDropdown(){
        return this.findElement(By.id(ID_SETUP_OPTIONS_DROPDOWN));
    }

    private WebElement findFreeEventOption(){
        return this.findElement(By.id(ID_FREE_EVENT_OPTION));
    }

    private WebElement findSetupTicketPriceOption(){
        return this.findElement(By.id(ID_SETUP_TICKET_PRICE_OPTION));
    }

    private WebElement findAllowTicketBuyersCheckbox(){
        return this.findElement(By.id(ID_ALLOW_TICKET_BUYERS_CHECKBOX));
    }

    private WebElement findAddTicketTypeButton(){
        return this.findElement(By.id(ID_ADD_TICKET_TYPE_BUTTON));
    }

    private WebElement findTicketTypeField(){
        return this.findElement(By.id(ID_TICKET_TYPE_FIELD));
    }

    private WebElement findTicketTypeDescField(){
        return this.findElement(By.id(ID_TICKET_TYPE_DESC_FIELD));
    }

    private WebElement findTicketTypeQtyField(){
        return this.findElement(By.id(ID_TICKET_TYPE_QTY_FIELD));
    }

    private WebElement findTicketTypeAmtField(){
        return this.findElement(By.id(ID_TICKET_TYPE_AMT_FIELD));
    }

    private WebElement findTicketTypeStatusDropdown(){
        return this.findElement(By.id(ID_TICKET_TYPE_STATUS_DROPDOWN));
    }

    private WebElement findTicketTypeActiveOption(){
        return this.findElement(By.id(ID_TICKET_TYPE_ACTIVE));
    }

    private WebElement findTicketTypeHiddenOption(){
        return this.findElement(By.id(ID_TICKET_TYPE_HIDDEN));
    }

    private WebElement findTicketTypeSaveButton(){
        return this.findElement(By.id(ID_TICKET_TYPE_SAVE_BUTTON));
    }

    private WebElement findTicketPriceNextButton(){
        return this.findElement(By.id(ID_TICKET_PRICE_NEXT_BUTTON));
    }

    private WebElement findCreateEventButton(){
        return this.findElement(By.id(ID_CREATE_EVENT_BUTTON));
    }

    private WebElement findFinishButton(){
        return this.findElement(By.id(ID_FINISH_BUTTON));
    }

    //region METHODS
    private void inputEventName(String eventName){
        this.sendKeys(this.findEventNameField(), eventName);
    }

    private void inputEventDescription(String desc){
        this.sendKeys(this.findEventDescriptionField(), desc);
    }

    private void inputEventContactName(String contactName){
        this.sendKeys(this.findEventContactNameField(), contactName);
    }

    private void inputEventContactPhone(String contactPhone){
        this.sendKeys(this.findEventContactPhoneField(), contactPhone);
    }

    private void inputEventContactEmail(String contactEmail){
        this.sendKeys(this.findEventContactEmailField(), contactEmail);
    }

    private void clickCreateEventNextButton(){
        this.click(this.findCreateEventNextButton());
    }

    public void inputCreateEventDetails(String eventName, String desc){
        this.inputEventName(eventName);
        this.inputEventDescription(desc);
    }
    public void inputEventContactDetails(String contactName, String contactPhone, String contactEmail){
        this.inputEventContactName(contactName);
        this.inputEventContactPhone(contactPhone);
        this.inputEventContactEmail(contactEmail);
        this.clickCreateEventNextButton();
    }

    public void clickVenueTemplateDrodpown(){
        this.click(this.findVenueTemplateDropdown());
    }

    private void inputVenueName(String venueName){
        this.sendKeys(this.findVenueNameField(), venueName);
    }

    private void inputVenueAddress1(String address1){
        this.sendKeys(this.findVenueAddress1Field(), address1);
    }

    private void inputVenueAddress2(String address2){
        this.sendKeys(this.findVenueAddress2Field(), address2);
    }

    private void inputVenueCity(String city){
        this.sendKeys(this.findVenueCityField(), city);
    }

    private void inputVenueState(String state){
        this.sendKeys(this.findVenueStateField(), state);
    }

    private void inputVenuePostCode(String postCode){
        this.sendKeys(this.findVenuePostCodeField(), postCode);
    }

    private void clickCreateVenueNextButton(){
        this.click(this.findCreateVenueNextButton());
    }

    private void inputSessionStartDate(String date){
        this.sendKeys(this.findSessionStartDateField(), date);
    }

    private void inputSessionStartTime(String time){
        this.deleteDateTime(this.findSessionStartTimeField());
        this.inputTime(time);
    }

    private void selectSessionStartTimeAmPm(String amPm){
        this.click(this.findSessionStartTimeAmPmDropdown());
        if (amPm.equals("AM")){
            this.click(this.findSessionTimeAm());
        }else {
            this.click(this.findSessionTimePm());
        }
    }

    private void clickSessionTimeNextButton(){
        this.click(this.findSessionTimeNextButton());
    }

    private void inputBookingStartDate(String date){
        this.deleteDateTime(this.findBookingStartDateField());
        this.sendKeys(this.findBookingStartDateField(), date);
    }

    private void inputBookingStartTime(String time){
        this.deleteDateTime(this.findBookingStartTimeField());
        this.inputTime(time);
    }

    private void selectBookingStartTimeAmpPm(String amPm){
        this.click(this.findBookingStartTimeDropdown());
        if (amPm.equals("AM")){
            this.click(this.findBookingStartTimeAm());
            }else {
            this.click(this.findBookingStartTimePm());
        }
    }

    private void inputBookingEndDate(String date){
        this.deleteDateTime(this.findBookingEndDateField());
        this.sendKeys(this.findBookingEndDateField(), date);
    }

    private void inputBookingEndTime(String time){
        this.deleteDateTime(findBookingEndTimeField());
        this.inputTime(time);
    }

    private void selectBookingEndTimeAmpPm(String amPm){
        this.click(this.findBookingEndTimeDropdown());
        if (amPm.equals("AM")){
            this.click(this.findBookingEndTimeAm());
        }else {
            this.click(this.findBookingEndTimePm());
        }
    }

    private void clickBookingWindowNextButton(){
        this.click(this.findBookingWindowNextButton());
    }

    private void clickSeatingDetailsNextButton(){
        this.click(this.findSeatingDetailsNextButton());
    }

    public void selectAllocationSeat(String rowNumber, String columnNumber){
        this.click(this.findSeatingAllocationTypeDropdown());
        this.click(this.findSeatingAllocationSeat());
        this.sendKeys(this.findSeatingRowNumber(), rowNumber);
        this.sendKeys(this.findSeatingColumnNumber(), columnNumber);
        this.clickSeatingDetailsNextButton();
    }

    public void selectAllocationSpace(String maxNumber){
        this.sendKeys(this.findSeatingMaxNumberOfAttendees(), maxNumber);
        this.clickSeatingDetailsNextButton();
    }

    private void clickTicketPriceNextButton(){
        this.click(this.findTicketPriceNextButton());
    }

    private void clickSetuptionsDropdown(){
        this.click(this.findSetupOptionsDropdown());
    }

    private void clickSetupTicketPriceOption(){
        this.click(this.findSetupTicketPriceOption());
    }

    private void clickAddTicketTypeButton(){
        this.click(this.findAddTicketTypeButton());
    }

    private void inputTicketType(String type){
        this.sendKeys(this.findTicketTypeField(), type);
    }

    private void inputTicketTypeDesc(String desc){
        this.sendKeys(this.findTicketTypeDescField(),desc);
    }

    private void inputTicketTypeQty(String qty){
        this.sendKeys(this.findTicketTypeQtyField(), qty);
    }

    private void inputTicketTypeAmt(String amt){
        this.sendKeys(this.findTicketTypeAmtField(), amt);
    }

    private void selectTicketTypeStatus(String status){
        this.click(this.findTicketTypeStatusDropdown());
        if (status.equals(TestData.STATUS_ACTIVE)){
            this.click(this.findTicketTypeActiveOption());
        }else{
            this.click(this.findTicketTypeHiddenOption());
        }
    }

    private void clickTicketTypeSaveButton(){
        this.click(this.findTicketTypeSaveButton());
    }

    private void clickCreateEventButton(){
        this.click(this.findCreateEventButton());
    }

    private void clickFinishButton(){
        this.click(this.findFinishButton());
    }
    //endregion

    //region HIGHLEVEL METHODS

    private void selectVenueOption(String venueTemplate){
        WebElement tableBody = this.findVenueTemplateOptionsTable().findElement(By.tagName("tbody"));
        List<WebElement> tableRows = tableBody.findElements(By.tagName("tr"));
        String option;
        for (WebElement tableRow : tableRows){
            option = tableRow.getText();
            if (option.equals(venueTemplate)){
                click(tableRow);
                break;
            }
        }
    }

    public void selectVenueTemplate(String template){
        this.clickVenueTemplateDrodpown();
        this.selectVenueOption(template);

    }

    public void inputNewVenueDetails(String name, String address1, String address2, String city, String state, String postcode){
        this.inputVenueName(name);
        this.inputVenueAddress1(address1);
        this.inputVenueAddress2(address2);
        this.inputVenueCity(city);
        this.inputVenueState(state);
        this.inputVenuePostCode(postcode);
        this.clickCreateVenueNextButton();
    }

    public void inputSessionTimeDetails(String date, String time, String amPm){
        this.inputSessionStartDate(date);
        this.inputSessionStartTime(time);
        this.selectSessionStartTimeAmPm(amPm);
        this.clickSessionTimeNextButton();
    }

    public void inputBookingWindowDetails(String startDate, String startTime, String startAmPm, String endDate, String endTime, String endAmPm){
        this.inputBookingStartDate(startDate);
        this.inputBookingStartTime(startTime);
        this.selectBookingStartTimeAmpPm(startAmPm);this.inputBookingEndDate(endDate);
        this.inputBookingEndTime(endTime);
        this.selectBookingEndTimeAmpPm(endAmPm);
        this.clickBookingWindowNextButton();
    }

    public void selectFreeEventOption(boolean allow){
        if(allow==true){
            this.click(this.findAllowTicketBuyersCheckbox());
        }
        this.clickTicketPriceNextButton();
    }
    public void selectSetupTicketPriceOption(String ticketType, String ticketTypeDesc, String ticketQty, String ticketAmt, String ticketStatus){
        this.clickSetuptionsDropdown();
        this.clickSetupTicketPriceOption();
        this.clickAddTicketTypeButton();
        this.sendKeys(this.findTicketTypeField(), ticketType);
        this.sendKeys(this.findTicketTypeDescField(), ticketTypeDesc);
        this.sendKeys(this.findTicketTypeQtyField(), ticketQty);
        this.sendKeys(this.findTicketTypeAmtField(), ticketAmt);
        this.selectTicketTypeStatus(ticketStatus);
        this.clickTicketTypeSaveButton();
        this.clickTicketPriceNextButton();
    }

    public void inputOptionsDetails(){
        //TODO: Waiting List and Keywords
        this.clickCreateEventButton();
        this.clickFinishButton();
    }


    //endregion


}
