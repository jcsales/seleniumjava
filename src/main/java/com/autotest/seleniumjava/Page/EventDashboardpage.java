package com.autotest.seleniumjava.Page;

import com.autotest.seleniumjava.WaitUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by jsales on 9/12/2017.
 */
public class EventDashboardpage extends BasePage {
    private static final String ID_SESSION_TIME_LINK = "ctl00_ContentPlaceHolder1_hlEventDate";
    private static final String ID_LOGOUT_BUTTON = "ctl00_pnlHeaderPane_lnkLogout";

    private WebElement findSessionTimeLink(){
        return this.findElement(By.id(ID_SESSION_TIME_LINK));
    }

    private WebElement findLogoutButton(){
        return this.findElement(By.id(ID_LOGOUT_BUTTON));
    }


    public String getEID(){
        WaitUtil.sleep(3000);
        String url = getWebDriver().getCurrentUrl();
        int length = url.length() - 4;
        String eid = url.substring(length);
        return eid;
    }

    public void clickSessionTimeLink(){
        this.click(this.findSessionTimeLink());
    }

    public void clickLogoutButton(){
        click(this.findLogoutButton());
    }


    //region HIGH LEVEL METHOD
    public void logout(String eid){
        this.goToEventDashboard(eid);
        this.clickLogoutButton();
    }


    //endregion



}
