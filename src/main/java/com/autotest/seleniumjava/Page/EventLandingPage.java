package com.autotest.seleniumjava.Page;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

/**
 * Created by jsales on 10/20/2017.
 */
public class EventLandingPage extends BasePage{
    private final static String ID_SESSION_BUTTON = "sessionsButtonEventPageLargeId_";
    private final static String ID_SESSION_TABLE_HEADING = "sessionsTableHeadingId";

    private WebElement findSessionTableHeading(){
        return this.findElement(By.id(ID_SESSION_TABLE_HEADING));
    }

    private WebElement findSessionButton(String sessionDate){
        String[] parts = sessionDate.split(" ");
        String session;
        if(parts[1].startsWith("0")){
            String newdate = parts[1].substring(1);
            session = parts[0]+" "+newdate+" "+parts[2]+" "+parts[3];
        }else{
            session = sessionDate;
        }
        WebElement parent = getParent(this.findSessionTableHeading());
        WebElement tbody = parent.findElement(By.tagName("table")).findElement(By.tagName("tbody"));
        int trSize = tbody.findElements(By.tagName("tr")).size();
        for(int i=0; i<=trSize; i++){
            String sessionDateTime = tbody.findElements(By.tagName("tr")).get(i).getText();
            if(sessionDateTime.contains(session)){
                return this.findElement(By.id(ID_SESSION_BUTTON+i));
            }
        }
        return null;
    }

    private WebElement findSessionStatus(String sessionDate){
        String[] parts = sessionDate.split(" ");
        String session;
        if(parts[1].startsWith("0")){
            String newdate = parts[1].substring(1);
            session = parts[0]+" "+newdate+" "+parts[2]+" "+parts[3];
        }else{
            session = sessionDate;
        }
        WebElement parent = getParent(this.findSessionTableHeading());
        WebElement tbody = parent.findElement(By.tagName("table")).findElement(By.tagName("tbody"));
        int trSize = tbody.findElements(By.tagName("tr")).size();
        for(int i=0; i<=trSize; i++){
            String sessionDateTime = tbody.findElements(By.tagName("tr")).get(i).getText();
            if(sessionDateTime.contains(session)){
                return tbody.findElements(By.tagName("tr")).get(i).findElements(By.tagName("td")).get(2);
            }
        }
        return null;
    }



    public void clickSessionSelectButton(String sessionDate){
        click(this.findSessionButton(sessionDate));
    }

    public boolean sessionButtonIsDisabled(String sessionDate){
        return !this.findSessionButton(sessionDate).isEnabled();
    }

    public boolean sessionButtonIsEnabled(String sessionDate){
        return this.findSessionButton(sessionDate).isEnabled();
    }

    public boolean sessionStatusIsCorrect(String sessionDate, String sessionStatus){
        String status = this.findSessionStatus(sessionDate).getText();
        return status.equals(sessionStatus);
    }
}
