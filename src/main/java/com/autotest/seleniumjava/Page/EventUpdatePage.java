package com.autotest.seleniumjava.Page;

import com.autotest.seleniumjava.TestData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by jsales on 9/12/2017.
 */
public class EventUpdatePage extends BasePage {

    //region PAGES
    private EventDashboardpage eventDashboardpage = new EventDashboardpage();


    //endregion

    //region CONSTANTS
    private static final String ID_ADD_SESSION_TIME_BUTTON = "ctl00_ContentPlaceHolder1_pnlSessions_btnAdd";
    private static final String ID_SESSION_STATUS_DROPDOWN = "ctl00_ContentPlaceHolder1_pgEventDate_ddlVisible_B-1";
    private static final String ID_SESSION_STATUS_CLOSED = "ctl00_ContentPlaceHolder1_pgEventDate_ddlVisible_DDD_L_LBI1T0";
    private static final String ID_SESSION_STATUS_FULL = "ctl00_ContentPlaceHolder1_pgEventDate_ddlVisible_DDD_L_LBI2T0";
    private static final String ID_SESSION_STATUS_HIDDEN = "ctl00_ContentPlaceHolder1_pgEventDate_ddlVisible_DDD_L_LBI3T0";
    private static final String ID_SESSION_BOOKING_START_DATE_FIELD = "ctl00_ContentPlaceHolder1_pgEventDate_deBookingStartDate_I";
    private static final String ID_SESSION_BOOKING_START_TIME_FIELD = "ctl00_ContentPlaceHolder1_pgEventDate_teBookingStartTime_I";
    private static final String ID_SESSION_BOOKING_END_DATE_FIELD = "ctl00_ContentPlaceHolder1_pgEventDate_deBookingEndDate_I";
    private static final String ID_SESSION_BOOKING_END_TIME_FIELD = "ctl00_ContentPlaceHolder1_pgEventDate_teBookingEndTime_I";

    private static final String ID_SESSION_BOOKING_END_TIME_DROPDOWN = "ctl00_ContentPlaceHolder1_pgEventDate_ddlBookingEndTimeAMPM_B-1";
    private static final String ID_SESSION_BOOKING_END_TIME_AM = "ctl00_ContentPlaceHolder1_pgEventDate_ddlBookingEndTimeAMPM_DDD_L_LBI0T0";
    private static final String ID_SESSION_BOOKING_END_TIME_PM = "ctl00_ContentPlaceHolder1_pgEventDate_ddlBookingEndTimeAMPM_DDD_L_LBI1T0";
    private static final String ID_SESSION_UPDATE_SAVE_BUTTON = "ctl00_ContentPlaceHolder1_pgEventDate_btnSave";
    private static final String ID_SESSION_START_DATE_FIELD = "ctl00_ContentPlaceHolder1_pgEventDate_deEventStartDate_I";
    private static final String ID_SESSION_START_TIME_FIELD = "ctl00_ContentPlaceHolder1_pgEventDate_teEventStartTime_I";
    private static final String ID_SESSION_START_AMPM_DROPDOWN = "ctl00_ContentPlaceHolder1_pgEventDate_ddlEventStartTimeAMPM_B-1";
    private static final String ID_SESSION_START_AM = "ctl00_ContentPlaceHolder1_pgEventDate_ddlEventStartTimeAMPM_DDD_L_LBI0T0";
    private static final String ID_SESSION_START_PM = "ctl00_ContentPlaceHolder1_pgEventDate_ddlEventStartTimeAMPM_DDD_L_LBI1T0";

    //endregion

    //region   ELEMENTS
    private WebElement findAddSessionTimeButton(){
        return this.findElement(By.id(ID_ADD_SESSION_TIME_BUTTON));
    }
    private WebElement findSessionTimeDropdown(){
        return this.findElement(By.id(ID_SESSION_START_AMPM_DROPDOWN));
    }
    private WebElement findSessionStatusClosed(){
        return this.findElement(By.id(ID_SESSION_STATUS_CLOSED));
    }

    private WebElement findSessionStatusFull(){
        return this.findElement(By.id(ID_SESSION_STATUS_FULL));
    }

    private WebElement findSessionStatusHidden(){
        return this.findElement(By.id(ID_SESSION_STATUS_HIDDEN));
    }

    private WebElement findSessionBookingStartDateField(){
        return this.findElement(By.id(ID_SESSION_BOOKING_START_DATE_FIELD));
    }

    private WebElement findSessionBookingStartTimeField(){
        return this.findElement(By.id(ID_SESSION_BOOKING_START_TIME_FIELD));
    }

    private WebElement findSessionUpdateSaveButton(){
        return this.findElement(By.id(ID_SESSION_UPDATE_SAVE_BUTTON));
    }

    private WebElement findSessionStartDateField(){
        return this.findElement(By.id(ID_SESSION_START_DATE_FIELD));
    }

    private WebElement findSessionStartTimeField(){
        return this.findElement(By.id(ID_SESSION_START_TIME_FIELD));
    }

    private WebElement findSessionStartTimeAm(){
        return this.findElement(By.id(ID_SESSION_START_AM));
    }

    private WebElement findSessionStartTimePm(){
        return this.findElement(By.id(ID_SESSION_START_PM));
    }

    private WebElement findSessionBookingEndDateField(){
        return this.findElement(By.id(ID_SESSION_BOOKING_END_DATE_FIELD));
    }

    private WebElement findSessionBookingEndTimeField(){
        return this.findElement(By.id(ID_SESSION_BOOKING_END_TIME_FIELD));
    }

    private WebElement findSessionBookingEndTimeDropdown(){
        return this.findElement(By.id(ID_SESSION_BOOKING_END_TIME_DROPDOWN));
    }

    private WebElement findSessionBookingEndTimeAm(){
        return this.findElement(By.id(ID_SESSION_BOOKING_END_TIME_AM));
    }

    private WebElement findSessionBookingEndTimePm(){
        return this.findElement(By.id(ID_SESSION_BOOKING_END_TIME_PM));
    }

    private WebElement findSessionStatusDropdown(){
        return this.findElement(By.id(ID_SESSION_STATUS_DROPDOWN));
    }

    //endregion

    //region CLICK and SENDKEYS
    private void clickAddSessionTimeButton(){
        this.click(this.findAddSessionTimeButton());
    }

    private void clickSessionTimeDropdown(){
        this.click(this.findSessionTimeDropdown());
    }

    private void clickSessionStatusDropdown(){
        this.click(this.findSessionStatusDropdown());
    }
    private void clickSessionStatusClosed(){
        this.click(this.findSessionStatusClosed());
    }

    private void clickSessionStatusFull(){
        this.click(this.findSessionStatusFull());
    }

    private void clickSessionStatusHidden(){
        this.click(this.findSessionStatusHidden());
    }

    private void clickSessionUpdateSaveButton(){
        this.click(this.findSessionUpdateSaveButton());
    }


    private void inputSessionBookingStartDate(String date){
        this.sendKeys(this.findSessionBookingStartDateField(), date);
    }

    private void inputSessionBookingStartTime(String time){
        this.sendKeys(this.findSessionBookingStartTimeField(), time);
    }

    private void inputSessionBookingEndtDate(String date) {
        this.deleteDateTime(this.findSessionBookingEndDateField());
        this.sendKeys(this.findSessionBookingEndDateField(), date);

    }

    private void inputSessionBookingEndTime(String time){
        this.deleteDateTime(this.findSessionBookingEndTimeField());
        this.inputTime(time);
    }

    public void clickSessionBookingEndTimeDropdown(){
        this.click(this.findSessionBookingEndTimeDropdown());
    }

    public void clickSessionBookingEndTimeAm(){
        this.click(this.findSessionBookingEndTimeAm());
    }

    public void clickSessionBookingEndTimePm(){
        this.click(this.findSessionBookingEndTimePm());
    }

    public void inputSessionStartDate(String date){
        this.deleteDateTime(this.findSessionStartDateField());
        this.sendKeys(this.findSessionStartDateField(), date);

    }

    public void inputSessionStartTime(String time){
        this.deleteDateTime(this.findSessionStartTimeField());
        this.inputTime(time);
    }

    public void clickSessionStartAm(){
        this.click(this.findSessionStartTimeAm());
    }

    public void clickSessionStartPm(){
        this.click(this.findSessionStartTimePm());
    }

    //endregion

    //region HIGH LEVEL METHODS
    private void selectSessionStatus(String status){
        this.clickSessionStatusDropdown();
        if(status.equals(TestData.STATUS_CLOSED)){
            this.clickSessionStatusClosed();
        }else if(status.equals(TestData.STATUS_FULL)){
            this.clickSessionStatusFull();
        }else if(status.equals(TestData.STATUS_HIDDEN)){
            this.clickSessionStatusHidden();
        }
    }

    private void selectSessionStartAmPm(String amPm){
        this.clickSessionTimeDropdown();
        if(amPm.equals(TestData.TIME_AM)){
            this.clickSessionStartAm();
        }else {
            this.clickSessionStartPm();
        }
    }

    private void selectSessionEndAmPm(String amPm){
        this.clickSessionBookingEndTimeDropdown();
        if(amPm.equals(TestData.TIME_AM)){
            this.clickSessionBookingEndTimeAm();
        }else {
            this.clickSessionBookingEndTimePm();
        }
    }

    public void inputSessionTimeDetails(String sessionStartDate, String sessionStartTime, String sessionAmPm,
                                        String bookingEndDate, String bookingStartTime, String bookingEndAmpPm, String status){
        this.inputSessionStartDate(sessionStartDate);
        this.inputSessionStartTime(sessionStartTime);
        this.selectSessionStartAmPm(sessionAmPm);
        this.inputSessionBookingEndtDate(bookingEndDate);
        this.inputSessionBookingEndTime(bookingStartTime);
        this.selectSessionEndAmPm(bookingEndAmpPm);
        this.selectSessionStatus(status);
        this.clickSessionUpdateSaveButton();
    }

    public void addSessionToEvent(String sessionStartDate, String sessionStartTime, String sessionAmPm, String status){
        this.eventDashboardpage.clickSessionTimeLink();
        this.clickAddSessionTimeButton();
        this.inputSessionTimeDetails(sessionStartDate, sessionStartTime, sessionAmPm, sessionStartDate, sessionStartTime,
                sessionAmPm, status);
    }

    //endregion
}
