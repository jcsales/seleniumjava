package com.autotest.seleniumjava.Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by jsales on 9/12/2017.
 */
public class LoginPage extends BasePage {
//    private static final String XPATH_USERNAME_FIELD = "//*[@id=\"loginForm\"]/input[1]";
//    private static final String XPATH_PASSWORD_FIELD = "//*[@id=\"loginForm\"]/input[2]";
//    private static final String XPATH_LOGIN_BUTTON = "//*[@id=\"loginForm\"]/input[3]";
    private static final String ID_LOGIN_DROPDOWN_BUTTON = "btnLogin";
    private static final String XPATH_EMAIL_LOGIN_FIELD = "//*[@id=\"loginForm\"]/input[1]";
    private static final String XPATH_PASSWORD_LOGIN_FIELD = "//*[@id=\"loginForm\"]/input[2]";
    private static final String XPATH_LOGIN_BUTTON = "//*[@id=\"loginForm\"]/div[1]";

    //region ELEMENTS
    private WebElement findEmailLoginField(){
        return this.findElement(By.xpath(XPATH_EMAIL_LOGIN_FIELD));
    }

    private WebElement findPasswordField(){
        return this.findElement(By.xpath(XPATH_PASSWORD_LOGIN_FIELD));
    }

    private WebElement findLoginButton(){
        return this.findElement(By.xpath(XPATH_LOGIN_BUTTON));
    }

    private WebElement findLoginDropdownButton(){
        return this.findElement(By.id(ID_LOGIN_DROPDOWN_BUTTON));
    }



    //endregion


    //region SENDKEYS AND CLICK
    private void inputEmailLogin(String username){
        sendKeys(this.findEmailLoginField(), username);
    }

    private void inputPassword(String password){
        sendKeys(this.findPasswordField(), password);
    }

    private void clickLoginButton(){
        click(this.findLoginButton());
    }

    private void clickLoginDropdownButton(){
        click(this.findLoginDropdownButton());
    }

    //endregion

    //region HIGHLEVEL METHODS
    public void loginAsEO(String username, String password){
        this.clickLoginDropdownButton();
        this.inputEmailLogin(username);
        this.inputPassword(password);
        this.clickLoginButton();
    }

    //endregion
}
