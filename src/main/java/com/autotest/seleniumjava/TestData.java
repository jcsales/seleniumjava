package com.autotest.seleniumjava;

/**
 * Created by jsales on 9/12/2017.
 */
public class TestData {
    public static final String USERNAME_EO = "tbtesting00@gmail.com";
    public static final String PASSWORD = "Hellotesting1";
    public static final String EVENT_NAME = "Braveheart";
    public static final String DATE_FORMAT_ddMMyyyy = "dd/MM/yyyy";
    public static final String DATE_FORMAT_DAY_DATE_MONTH_YEAR = "EEEE dd MMMM yyyy";
    public static final String BOOKING_START_TIME = "06:00";

    public static final String EVENT_DESCRIPTION = "Testing event creation";
    public static final String CREATE_EVENT_CONTACT_NAME = "Susan Smith";
    public static final String CREATE_EVENT_CONTACT_PHONE = "1234567";
    public static final String CREATE_EVENT_CONTACT_EMAIL = "tbtesting00@gmail.com";
    public static final String NEW_VENUE_TEMPLATE = "New Venue";
    public static final String NEW_VENUE_NAME = "New Venue";
    public static final String NEW_VENUE_ADDRESS1 = "600 Chapel Street";
    public static final String NEW_VENUE_ADDRESS2 = "";
    public static final String NEW_VENUE_CITY = "South Yarra";
    public static final String NEW_VENUE_STATE = "Victoria";
    public static final String NEW_VENUE_POSTCODE = "3141";
    public static final String SESSION_START_TIME = "06:00";
    public static final String TIME_AM = "AM";
    public static final String TIME_PM = "PM";

    public static final String ALLOCATION_SEAT = "Seat";
    public static final String ALLOCATION_SPACE = "Space";
    public static final String STATUS_ACTIVE = "Active";
    public static final String STATUS_HIDDEN = "Hidden";
    public static final String STATUS_CLOSED = "Closed";
    public static final String STATUS_OPEN = "OPEN";
    public static final String STATUS_FULL = "Full";
    public static final String TICKET_TYPE_ALL = "All";
    public static final String TICKET_TYPE_DESC = "This is for all";
    public static final String TICKET_TYPE_QTY_1 = "1";
    public static final String TICKET_TYPE_AMT_30 = "30";
    public static final String BROWSER_FF = "gecko";
    public static final String BROWSER_IE = "ie";
    public static final String BROWSER_CHROME = "chrome";
    public static final String BROWSER_EDGE = "edge";


    public static final String URL_EVENT_DASHBOARD = "https://portal.tst1.bookwana.com/Event/EventDashboard.aspx?eid=";
    public static final String URL_EVENT_LANDING_PAGE = "https://www.tst1.bookwana.com/";


}
