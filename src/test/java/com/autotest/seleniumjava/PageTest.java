package com.autotest.seleniumjava;

import com.autotest.seleniumjava.Page.BasePage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

/**
 * Created by LENOVO on 08/07/2017.
 */
public class PageTest extends BaseTest{

    private BasePage page;

    @Before
    public void beforeTest(){
        super.beforeTest();
        this.page = new BasePage();
    }

    @After
    public void afterTest(){
        super.afterTest();
    }

    @Test
    public void test01() {
        this.page.switchToRussianLang()
                .goToElecEngrSection()
                .clickSearchLink()
                .inputTextToSearch("Computer")
                .selectSubheading("Продают")
                .clickSearchButton()
                .sortByPrice()
                .selectTransactionType("Продажа")
                .clickAdvancedSearchLink()
                .inputMinPrice("160")
                .inputMaxPrice("300")
                .clickSearchButton();
        //index starts with 1
        this.page.selectRandomAdsByIndex(1);
        this.page.selectRandomAdsByIndex(2);
        this.page.selectRandomAdsByIndex(3);
        this.page.addToMemo();
        this.page.openBookmarks();
        this.page.validateIfAdsMatch();

    }

    @Test
    public void test02(){
        sendKeys(findElement(By.xpath("//*[@id=\"loginForm\"]/input[1]")), "tbtesting00@gmail.com");
        sendKeys(findElement(By.name("txtPassword")), "password1");
        click(findElement(By.xpath("//*[@id=\"loginForm\"]/input[3]")));
//        getWebDriver().get("https://portal.tst1.bookwana.com/Event/EventUpdateEventDate.aspx?eid=7151");
        click(findElement(By.id("ctl00_ContentPlaceHolder1_hlTransactionReport")));
        click(findElement(By.id("ctl00_ContentPlaceHolder1_btnGenerateReport")));


    }


}
