package com.autotest.seleniumjava;

import com.autotest.seleniumjava.Page.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import java.text.DateFormat;

/**
 * Created by jsales on 9/11/2017.
 */
public class TbSmokeTest extends TestMethodProvider {
    private BasePage page;
    private TestMethodProvider test;
    private LoginPage loginPage;
    private EventDashboardpage eventDashboardpage;
    private EventLandingPage eventLandingPage;

    private String eid = new String();

    @Before
    public void beforeTest() {
        super.beforeTest();
        this.page = new BasePage();
        this.test = new TestMethodProvider();
        this.loginPage = new LoginPage();
        this.eventDashboardpage = new EventDashboardpage();
        this.eventLandingPage = new EventLandingPage();
        this.test.testInitialize();
}

    @After
    public void afterTest() {
        super.afterTest();
    }


    @Test
    public void C107() {
        eid = this.test.createEvent();
//        this.loginPage.loginAsEO(TestData.USERNAME_EO, TestData.PASSWORD);
        this.goToEventDashboard(eid);
        this.test.addSessionTimeToEvent(TestData.STATUS_CLOSED, 2);
        this.eventDashboardpage.logout(eid);
        this.goToEventLandingPageAsTicketBuyer(eid);
        String sessionDate1 = this.formatDate(1, TestData.DATE_FORMAT_DAY_DATE_MONTH_YEAR);
        String sessionDate2 = this.formatDate(2, TestData.DATE_FORMAT_DAY_DATE_MONTH_YEAR);
        this.eventLandingPage.sessionButtonIsEnabled(sessionDate1);
        this.eventLandingPage.sessionStatusIsCorrect(sessionDate1,TestData.STATUS_OPEN);
        this.eventLandingPage.sessionButtonIsDisabled(sessionDate2);
        this.eventLandingPage.sessionStatusIsCorrect(sessionDate2, TestData.STATUS_CLOSED);
    }

    @Test
    public void C115(){
        eid = this.test.createEvent();
        this.goToEventDashboard(eid);
    }

}
