package com.autotest.seleniumjava;

import com.autotest.seleniumjava.Page.*;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by jsales on 9/12/2017.
 */
public class TestMethodProvider extends BaseTest {
    private LoginPage loginPage;
    private AccountDashboardPage accountDashboardPage;
    private CreateEventPage createEventPage;
    private EventDashboardpage eventDashboardpage;
    private EventUpdatePage eventUpdatePage;

    public void testInitialize(){
        this.loginPage = new LoginPage();
        this.accountDashboardPage = new AccountDashboardPage();
        this.createEventPage = new CreateEventPage();
        this.eventDashboardpage = new EventDashboardpage();
        this.eventUpdatePage = new EventUpdatePage();
    }

    public String createEvent(){
        this.loginPage.loginAsEO(TestData.USERNAME_EO, TestData.PASSWORD);
        this.accountDashboardPage.clickCreateEventLink();
        String str = this.generateRandomNumbers(4);
        this.createEventPage.inputCreateEventDetails(TestData.EVENT_NAME + " " + str, TestData.EVENT_DESCRIPTION);
        this.createEventPage.inputEventContactDetails(TestData.CREATE_EVENT_CONTACT_NAME, TestData.CREATE_EVENT_CONTACT_PHONE, TestData.CREATE_EVENT_CONTACT_EMAIL);
        this.createEventPage.selectVenueTemplate(TestData.NEW_VENUE_TEMPLATE);
        this.createEventPage.inputNewVenueDetails(TestData.NEW_VENUE_NAME, TestData.NEW_VENUE_ADDRESS1, TestData.NEW_VENUE_ADDRESS2,
                TestData.NEW_VENUE_CITY, TestData.NEW_VENUE_STATE, TestData.NEW_VENUE_POSTCODE);
        this.createEventPage.inputSessionTimeDetails(formatDate(1), TestData.SESSION_START_TIME, TestData.TIME_PM);
        this.createEventPage.inputBookingWindowDetails(formatDate(0), TestData.BOOKING_START_TIME, TestData.TIME_AM, formatDate(1), TestData.SESSION_START_TIME, TestData.TIME_PM);
        this.createEventPage.selectAllocationSeat("5", "10");
        this.createEventPage.selectSetupTicketPriceOption(TestData.TICKET_TYPE_ALL, TestData.TICKET_TYPE_DESC, TestData.TICKET_TYPE_QTY_1, TestData.TICKET_TYPE_AMT_30, TestData.STATUS_ACTIVE);
        this.createEventPage.inputOptionsDetails();
        return this.eventDashboardpage.getEID();
    }

    public void addSessionTimeToEvent(String status, int daysToAdd){
        this.eventUpdatePage.addSessionToEvent(formatDate(daysToAdd), TestData.SESSION_START_TIME, TestData.TIME_PM, status);
        if(status.equals(TestData.STATUS_CLOSED)){
            this.clickOkOnConfirmation();
        }
    }



    private String generateRandomNumbers(int count){
        return RandomStringUtils.randomNumeric(count);

    }

}
